"""base : definitions and fundamental classes, functions
"""
from enum import Enum

class loraSensorType(Enum):
    """Enumeration to represent every type of sensing device
    If the data payload changes structure between two same devices (like a firmware update) or the data source requires different handling (like a factory calibration value) add an use another value for the sensor type
    """
    rhf1s001 = "rhf1s001"
    waspmoteOEM = "waspmoteOEM"
    libeliumWeatherStation = "libeliumWeatherStation" #: used in fistur, only use this with the same firmware
    skycld1 = "skycld1"
    skyagr1 = "skyagr1"
    skyenr1 = "skyenr1"
    mcisoil = "mcisoil"
    mciTH = "mciTH"
    libeliumTempSoil = "libeliumTempSoil" #: waspmote with temp sensor and watermark
    davisVantagePro2SantaJulia = "DavisVantagePro2SantaJulia" #: davis system installed in Santa Julia (VantagePro2 + 3xTH)
    davisTH = "davisTH"

class agroDataType(Enum):
    """Enumeration to represent every type of data registered
    must be synced with sensorlist

    :param variable: string with the official name for the variable, must be the entry for the output json
    :param name: string with the name of the data, to be shown on top of a plot in spanish
    :param unit: string with the label for the plot
    """
    temperatureC = { "variable": "temperatureC", "name" : "Temperatura", "unit" : "°C" }
    humidityRH = { "variable": "humidityRH", "name" : "Humedad", "unit" : "%" }
    pressurePa = { "variable": "pressurePa", "name" : "Presión Atmosférica", "unit" : "Pa" }
    radiationpar = { "variable": "radiationpar", "name" : "Radiación Solar", "unit" : "m-2 s-1" }
    rainAccpPerHour = { "variable": "rainAccpPerHour", "name" : "Precipitaciones", "unit" : "mm" }
    battery = { "variable": "battery", "name" : "Batería", "unit" : "%" }
    vane16 = { "variable": "vane16", "name" : "Dirección del Viento", "unit" : "Orientación" }
    windkmh = { "variable": "windkmh", "name" : "Velocidad del Viento", "unit" : "km/h" }
    radiationPower = { "variable": "radiationPower", "name" : "Radiación Solar", "unit" : "W m-2" }
    uvIndex = { "variable": "uvIndex", "name" : "Indice UV", "unit" : "" }
    rainAccpPeriod = { "variable": "rainAccpPeriod", "name" : "Precipitaciones", "unit" : "mm" }
    soilHumidityRH = { "variable": "soilHumidityRH", "name" : "Humedad de suelo", "unit" : "%" }
    soilHumidityV = { "variable": "soilHumidityV", "name" : "Humedad de suelo", "unit" : "V" }
    soilHumidityCb = { "variable": "soilHumidityCb", "name" : "Humedad de suelo", "unit" : "cb" }

class parserOutputType(Enum):
    """Enumeration to represent output types of a parser
    """
    xlsx = "xlsx" #excel table for analysis and pretty print
    json = "json" #abstract backup format


class loraDevice():
    """Object to represent every sensor, one instance for every sensor

    :param deveui: string, 16 char long, uppercase. Must be the unique, identifier of the device
    :param sensorType: loraSensorType, type of the sensor
    :param datarate: nominal period (milliseconds) between messages, 0 if it is not a periodic sensor
    """
    def __init__(self, deveui, sensorType, datarate):
        self.deveui = deveui
        self.sensorType = sensorType
        self.datarate = datarate