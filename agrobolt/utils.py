"""utils : standalone functions that can be useful in a lot of cases
"""
import json
import os
import dateutil.parser
import agrobolt.base as base

def allTheDevices():
    """Returns a data structure containing all the sensors currently registered
    The data structure uses loraDevice instances
    """
    devicesdict = {}
    devicesdict["8CF9574F0000050E"] = base.loraDevice("8CF9574F0000050E", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000533"] = base.loraDevice("8CF9574000000533", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000052D"] = base.loraDevice("8CF957400000052D", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000539"] = base.loraDevice("8CF9574000000539", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000050C"] = base.loraDevice("8CF957400000050C", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000529"] = base.loraDevice("8CF9574000000529", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000525"] = base.loraDevice("8CF9574000000525", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000514"] = base.loraDevice("8CF9574000000514", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000051D"] = base.loraDevice("8CF957400000051D", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000522"] = base.loraDevice("8CF9574000000522", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000052A"] = base.loraDevice("8CF957400000052A", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000518"] = base.loraDevice("8CF9574000000518", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000513"] = base.loraDevice("8CF9574000000513", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000520"] = base.loraDevice("8CF9574000000520", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000512"] = base.loraDevice("8CF9574000000512", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000549"] = base.loraDevice("8CF9574000000549", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000523"] = base.loraDevice("8CF9574000000523", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000524"] = base.loraDevice("8CF9574000000524", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000528"] = base.loraDevice("8CF9574000000528", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000510"] = base.loraDevice("8CF9574000000510", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000009531"] = base.loraDevice("8CF9574000009531", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000051B"] = base.loraDevice("8CF957400000051B", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000051A"] = base.loraDevice("8CF957400000051A", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000054A"] = base.loraDevice("8CF957400000054A", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000530"] = base.loraDevice("8CF9574000000530", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000052F"] = base.loraDevice("8CF957400000052F", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000534"] = base.loraDevice("8CF9574000000534", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000515"] = base.loraDevice("8CF9574000000515", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000516"] = base.loraDevice("8CF9574000000516", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000527"] = base.loraDevice("8CF9574000000527", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000051F"] = base.loraDevice("8CF957400000051F", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000053F"] = base.loraDevice("8CF957400000053F", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000053E"] = base.loraDevice("8CF957400000053E", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000511"] = base.loraDevice("8CF9574000000511", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000519"] = base.loraDevice("8CF9574000000519", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000526"] = base.loraDevice("8CF9574000000526", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000052C"] = base.loraDevice("8CF957400000052C", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000535"] = base.loraDevice("8CF9574000000535", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000050B"] = base.loraDevice("8CF957400000050B", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000696"] = base.loraDevice("8CF9574000000696", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF95740000009E8"] = base.loraDevice("8CF95740000009E8", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF95740000009E9"] = base.loraDevice("8CF95740000009E9", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF95740000009EC"] = base.loraDevice("8CF95740000009EC", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF95740000009B6"] = base.loraDevice("8CF95740000009B6", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000054D"] = base.loraDevice("8CF957400000054D", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000580"] = base.loraDevice("8CF9574000000580", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000053C"] = base.loraDevice("8CF957400000053C", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF957400000050F"] = base.loraDevice("8CF957400000050F", base.loraSensorType.rhf1s001, 120)
    devicesdict["8CF9574000000521"] = base.loraDevice("8CF9574000000521", base.loraSensorType.rhf1s001, 120)
    devicesdict["61F12E66387A6E44"] = base.loraDevice("61F12E66387A6E44", base.loraSensorType.libeliumWeatherStation, 600)
    devicesdict["4E7D9ED6255E1ABD"] = base.loraDevice("4E7D9ED6255E1ABD", base.loraSensorType.libeliumWeatherStation, 300)
    devicesdict["81CB1EB273F356C6"] = base.loraDevice("81CB1EB273F356C6", base.loraSensorType.libeliumWeatherStation, 600)
    devicesdict["3CF46E4CA8527D70"] = base.loraDevice("3CF46E4CA8527D70", base.loraSensorType.davisVantagePro2SantaJulia, 600)
    devicesdict["3CF46E4CA8527D71"] = base.loraDevice("3CF46E4CA8527D71", base.loraSensorType.davisTH, 600)
    devicesdict["3CF46E4CA8527D72"] = base.loraDevice("3CF46E4CA8527D72", base.loraSensorType.davisTH, 600)
    devicesdict["3CF46E4CA8527D73"] = base.loraDevice("3CF46E4CA8527D73", base.loraSensorType.davisTH, 600)
    devicesdict["454052000000023B"] = base.loraDevice("454052000000023B", base.loraSensorType.skycld1, 600)
    devicesdict["4540520000000239"] = base.loraDevice("4540520000000239", base.loraSensorType.skyenr1, 600)
    devicesdict["4540520000000238"] = base.loraDevice("4540520000000238", base.loraSensorType.skyagr1, 900)
    devicesdict["4540520000000237"] = base.loraDevice("4540520000000237", base.loraSensorType.skyagr1, 900)
    devicesdict["454052000000023C"] = base.loraDevice("454052000000023C", base.loraSensorType.skyagr1, 900)
    devicesdict["0004A30B0023024C"] = base.loraDevice("0004A30B0023024C", base.loraSensorType.mcisoil, 60)
    devicesdict["0004A30B00235474"] = base.loraDevice("0004A30B00235474", base.loraSensorType.mcisoil, 60)
    devicesdict["0004A30B0022C9C2"] = base.loraDevice("0004A30B0022C9C2", base.loraSensorType.mciTH, 60)
    devicesdict["0004A30B002357B4"] = base.loraDevice("0004A30B002357B4", base.loraSensorType.mciTH, 60)
    devicesdict["7742EC8E36889D56"] = base.loraDevice("7742EC8E36889D56", base.loraSensorType.libeliumTempSoil, 60)
    devicesdict["8CF95740000009F3"] = base.loraDevice("8CF95740000009F3", base.loraSensorType.rhf1s001, 120)
    return devicesdict

def loadJSONS(dir_path, extension):
    """Walks a directory and parses every line of every file for a valid JSON string

    :param dir_path: A string, absolute path of the directory to use
    :param extension: file extension to be considered, include the . in the name
    :return: list of dicts, every one is a JSON entry in the files
    """
    f = []
    jsoncontainer = []
    print(dir_path)
    for (dirpath, dirnames, filenames) in os.walk(dir_path):  # general walk, only files in top directory
        f.extend(filenames)
        break
    f.sort()  #fileextension
    for filename in f:
        print(filename)
        if filename[-len(extension):] == extension:  # if filename fits,parse it and append every json line in jsoncontainer
            print("File name match : True")
            with open(os.path.join(dir_path,filename), "r") as datafile:
                line = datafile.readline()
                while line:
                    try:
                        jsoncontainer.append(json.loads(line))
                    except Exception as e:
                        print("INVALID LINE")
                    line = datafile.readline()
    return jsoncontainer

def Watermark_FTocb(hertz):
    """static function to decode from a watermark frequency to cb

    :param hertz: hertz reported by the sensor
    :return: centibar conversion by the watermark
    """
    return (150390-19.74*hertz)/(2.8875*hertz-137.5)

def normalizeTimestamp(entryvalue):
    """returns a milliseconds epoch timestamp string for the timestamp entered as argument, regardless if it is in seconds, or as number

    :param entryvalue: value to be converted
    :return: milliseconds epoch timestamp string
    """
    entry = entryvalue
    if type(entry) is int or type(entry) is float:
        entry = str(int(entry))
    if len(entry) == 10:
        entry = entry + "000"
    return entry

def dateToTimestamp(entrydate):
    """receives a parseable date string (in human), to be converted to a milliseconds epoch timestamp string

    :param entrydate: UTC date to be entered, compatible with dateutil.parser eg: "23/09/2017 13:45"

    :return: milliseconds epoch timestamp string
    """
    return normalizeTimestamp(dateutil.parser.parse(entrydate).timestamp())