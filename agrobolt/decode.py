"""decode : transforms payload strings to data structures
"""
import agrobolt.base as base
import agrobolt.utils as utils

def readGeneric(payload, sensortype):
    """Wrapper to select a data decoder, given a raw payload and a sensor type from the loraSensorType Enum

    :param payload: string containing the full payload
    :param sensortype: type of sensor, if it is a variation of a known sensor, create a new type and entry

    :return: a list containing a dict per value, or an exception if the type is invalid or the decoding function doesn't exist. It is the "values" field of a standard info message
    """
    # dictionary for switch statement, add an entry for every extra sensor type
    switch_dictionary = {
        base.loraSensorType.rhf1s001 : rhf1s001,
        base.loraSensorType.waspmoteOEM : None,
        base.loraSensorType.libeliumWeatherStation : libeliumWeatherStation,
        base.loraSensorType.skyagr1 : skyagr1,
        base.loraSensorType.skyenr1 : None,
        base.loraSensorType.skycld1 : skycld1,
        base.loraSensorType.mcisoil : mcisoil,
        base.loraSensorType.mciTH : mciTH,
        base.loraSensorType.libeliumTempSoil : libeliumTempSoil,
        base.loraSensorType.davisVantagePro2SantaJulia : None,
        base.loraSensorType.davisTH : None
    }
    if len(payload) == 0:
        return []
    return switch_dictionary[sensortype](payload)

def addMeasurementToValues(targetlist, datatype, value):
    """Simple wrapper to add a dict name/value to an existing list

    :param targetlist: list object to use
    :param datatype: agroDataType instance
    :param value: value to be inserted, will be inserted as a string

    :returns: targetlist, but modifies the original object, no need to use the value
    """
    targetlist.append({"name": datatype.value["variable"],"value": str(value)})
    return targetlist


def rhf1s001(payload): # decode from payload string
    """Extracts temperature and humidity from a RisingHF TH sensor, saturates the humidity to 0% and 100%

    :param payload: string containing the full payload

    :return: a dict containing the extracted values
    """
    temp = str(-46.85+(int(payload[4:6] + payload[2:4], 16)*175.72)/65536)
    hum = str(-6+(int(payload[6:8],16)*125)/256)
    if float(hum) > 100:
            hum = "100.0"
    if float(hum) < 0:
            hum = "0.0"
    returnvalues = []
    addMeasurementToValues(returnvalues, base.agroDataType.temperatureC, temp)
    addMeasurementToValues(returnvalues, base.agroDataType.humidityRH, hum)
    return returnvalues

def libeliumWeatherStation(payload): # decode from payload string (include parselorawanframe function here)
    """Extracts temperature, humidity, pressure, wind, rain and radiation information from a Libelium Weather Station
    Firmware is original RichardStationCodePYP.pde
    KNOWN BUG: MSB of atmospheric pressure is not transmitted, a sum of 80500 bar is added for correction

    :param payload: string containing the full payload

    :return: a dict containing the extracted values
    """
    bytes = [payload[i:i+2] for i in range(0, len(payload), 2)]
    bytes[0] = "".join(bytes[0:2])
    bytes[4] = "".join(bytes[4:6])
    del bytes[1]
    del bytes[4]
    relValues=[]
    for i in range(0,len(bytes)):
        relValues.append(int(bytes[i], 16))
    absValues = {}
    absValues["temp"] = relValues[0]*125.0/65536.0-40
    absValues["humd"] = relValues[1]*100.0/255.0
    absValues["pres"] = relValues[2]*(110000.0-30000.0)/255.0 + 80500
    absValues["rad"] = relValues[3]*6855.0/65536.0
    absValues["anemometer"] = relValues[4]*240/255
    absValues["vane"] = relValues[5]
    absValues["rainfall_ch"] = relValues[6]*70/255
    absValues["batteryLevel"] = relValues[7]*100/255
    absValues["savedToSDCard"] = relValues[8]
    returnvalues = []
    addMeasurementToValues(returnvalues, base.agroDataType.temperatureC, absValues["temp"])
    addMeasurementToValues(returnvalues, base.agroDataType.humidityRH, absValues["humd"])
    addMeasurementToValues(returnvalues, base.agroDataType.pressurePa, absValues["pres"])
    addMeasurementToValues(returnvalues, base.agroDataType.radiationpar, absValues["rad"])
    addMeasurementToValues(returnvalues, base.agroDataType.windkmh, absValues["anemometer"])
    addMeasurementToValues(returnvalues, base.agroDataType.vane16, absValues["vane"])
    addMeasurementToValues(returnvalues, base.agroDataType.rainAccpPerHour, absValues["rainfall_ch"])
    addMeasurementToValues(returnvalues, base.agroDataType.battery, absValues["batteryLevel"])
    return returnvalues

def skyagr1(payload): # decode from payload string
    """Extracts temperature and soil humidity from a Skysense SKYAGR1 sensor

    :param payload: string containing the full payload

    :return: a dict containing the extracted values
    """
    temp = float(int(payload[0:4],16))/10
    soilhum = -0.2017*(int(payload[4:8],16)) + 192.96
    returnvalues = []
    addMeasurementToValues(returnvalues, base.agroDataType.temperatureC, temp)
    addMeasurementToValues(returnvalues, base.agroDataType.soilHumidityRH, soilhum)
    return returnvalues

def skycld1(payload): # decode from payload string
    """Extracts temperature and humidity from a Skysense SKYENV1 (or SKYCLD1)

    :param payload: string containing the full payload

    :return: a dict containing the extracted values
    """
    temp = float(int(payload[0:4],16))/10
    hum = float(int(payload[4:8],16))/10
    returnvalues = []
    addMeasurementToValues(returnvalues, base.agroDataType.temperatureC, temp)
    addMeasurementToValues(returnvalues, base.agroDataType.humidityRH, hum)
    return returnvalues


def mciTH(payload): # decode from payload string
    """Extracts temperature and humidity from a MCI mciTH sensor

    :param payload: string containing the full payload

    :return: a dict containing the extracted values
    """
    temp = float(int(payload[2:6],16))/100
    hum = float(int(payload[6:10],16))/100
    returnvalues = []
    addMeasurementToValues(returnvalues, base.agroDataType.temperatureC, temp)
    addMeasurementToValues(returnvalues, base.agroDataType.humidityRH, hum)
    return returnvalues


def mcisoil(payload): # decode from payload string
    """Extracts soil humidity from a MCI mciSoil sensor

    :param payload: string containing the full payload

    :return: a dict containing the extracted values
    """
    soilhum = int(payload[2:6],16)*0.00818
    returnvalues = []
    addMeasurementToValues(returnvalues, base.agroDataType.soilHumidityV, soilhum)
    return returnvalues


def libeliumTempSoil(payload): # decode from payload string (include parselorawanframe function here)
    """Extracts temperature, humidity, pressure and soil humidity information from a Test Libelium Weather Station
    Firmware is original TestStation_TempSoil.pde
    KNOWN BUG: MSB of atmospheric pressure is not transmitted, a sum of 80500 bar is added for correction

    :param payload: string containing the full payload

    :return: a dict containing the extracted values
    """
    bytes = [payload[i:i+2] for i in range(0, len(payload), 2)]
    bytes[0] = "".join(bytes[0:2])
    bytes[4] = "".join(bytes[4:6])
    del bytes[1]
    del bytes[4]
    relValues=[]
    for i in range(0,len(bytes)):
        relValues.append(int(bytes[i], 16))
    absValues = {}
    absValues["temp"] = relValues[0]*125.0/65536.0-40
    absValues["humd"] = relValues[1]*100.0/255.0
    absValues["pres"] = relValues[2]*(110000.0-30000.0)/255.0 + 80500
    absValues["soilcb"] = utils.Watermark_FTocb(relValues[3]*15000.0/65536.0)
    absValues["batteryLevel"] = relValues[7]*100/255
    absValues["savedToSDCard"] = relValues[8]
    returnvalues = []
    addMeasurementToValues(returnvalues, base.agroDataType.temperatureC, absValues["temp"])
    addMeasurementToValues(returnvalues, base.agroDataType.humidityRH, absValues["humd"])
    addMeasurementToValues(returnvalues, base.agroDataType.pressurePa, absValues["pres"])
    addMeasurementToValues(returnvalues, base.agroDataType.battery, absValues["batteryLevel"])
    addMeasurementToValues(returnvalues, base.agroDataType.soilHumidityCb, absValues["soilcb"])
    return returnvalues