"""parsing : to read datafiles and output/format/analyze data
"""
from openpyxl import Workbook
import json
import time
import agrobolt.base as base
import agrobolt.utils as utils
import agrobolt.decode as decode
import agrobolt.web as web

def generalParser(dir_path, whitelist=[], extension=".json", outtype=None, start_date=None, end_date=None):
    """Entry point for the parsing functionality

    :param dir_path: absolute path of the directory to be parsed, without a final slash or backslash
    :param whitelist: dict containing sensors to be considered, an empty dict means to consider all existing sensors, dict must have key:pairs as DEVEUI:writeable_name
    :param extension: file extension to be considered, include the . in the name
    :param outtype: parserOutputType instance for the output type of the parsing to be done, None is no output
    :param start_date: start date to filter, a string will be parsed, a number will be considered a datetime, None is consider messages from the earliest available
    :param end_date: end date to filter, a string will be parsed, a number will be considered a datetime, None is consider messages to the latest available
    """
    allTheDevices = utils.allTheDevices()
    #load up all jsons
    jsoncontainer = utils.loadJSONS(dir_path, extension)
    #get the start and end timestamps
    start_timestamp = 0
    end_timestamp = 999999999999999
    if start_date:
        if type(start_date) is string:
            start_timestamp = int(utils.dateToTimestamp(start_date))
        else:
            start_timestamp = int(utils.normalizeTimestamp(start_date))
    if end_date:
        if type(end_date) is string:
            end_timestamp = int(utils.dateToTimestamp(end_date))
        else:
            end_timestamp = int(utils.normalizeTimestamp(end_date))
    #DEVEUI filtering
    dontfilteranyDEVEUI = len(whitelist) == 0
    #main loop, go through every entry
    filteredcontainer = {}
    print("Number of entries")
    print(len(jsoncontainer))
    for entry in jsoncontainer:
        if "cmd" in entry.keys() and entry["cmd"] == "rx": #is a lorawan data payload
            datalinetimestamp = int(utils.normalizeTimestamp(entry["ts"]))
            if datalinetimestamp > start_timestamp and datalinetimestamp < end_timestamp: #first filter, date
                if dontfilteranyDEVEUI or entry["EUI"] in whitelist.keys(): #second filter deveui
                    #at this stage the sensor data is valid and matches all available criteria, create or add entry
                    if entry["EUI"] not in filteredcontainer.keys():
                        filteredcontainer[entry["EUI"]] = {}
                    filteredcontainer[entry["EUI"]][entry["ts"]] = entry["data"]
    #payloads already filtered at this point
    print("Amount of IDs")
    print(len(filteredcontainer))
    if outtype == base.parserOutputType.json:
        print("JSON type output")
        for deveui in filteredcontainer.keys(): #every key detected
            print(deveui)
            sensortype = allTheDevices[deveui].sensorType
            with open(deveui + "_" + str(time.time()) +".json", 'w') as outfile:
                outfile.write("{\"devices\": [") 
                for filteredtimestamp in filteredcontainer[deveui].keys(): #every entry
                    values = decode.readGeneric(filteredcontainer[deveui][filteredtimestamp], sensortype)
                    jsondict = web.buildAbstractMessage(values, deveui, filteredtimestamp, sensortype, payload = filteredcontainer[deveui][filteredtimestamp])
                    outfile.write(json.dumps(jsondict) + "\n")
                outfile.write("]}")
    if outtype == base.parserOutputType.xlsx:
        pass



