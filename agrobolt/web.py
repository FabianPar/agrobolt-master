"""web: message builders and web managers
"""
import agrobolt.base as base

def buildAbstractMessage(values, deveui, timestamp, sensortype, battery=100, payload=""): 
    """Creates a dict compatible with abstract data messages, when dumped with json.dumps creates a valid json payload

    :param values: dict containing the measurements to be included
    :param deveui: string containing the unique identifier
    :param timestamp: milliseconds timestamp to be used
    :param sensortype: loraSensorType instance of this sensor
    :param payload: raw payload of this message
    """
    final_dict = {}
    final_dict["device"] = deveui
    final_dict["measurement_time"] = timestamp
    final_dict["battery_level"] = 100
    final_dict["payload"] = payload
    measurements = []
    final_dict["sensors"] = measurements
    this_sensor = {"name": deveui, "sensor_type" : sensortype.value, "values" : values}
    measurements.append(this_sensor)
    return final_dict
