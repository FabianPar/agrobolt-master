"""
Package Agrobolt - Module definitions

base : definitions, classes and Enums
utils : static utilites, to be used and reused in other modules
decode : data decoding and conversion, transform sensor raw measurements to processed and useful data
parsing : data parsing from backup files, for backups or calc sheets
web : data transmission, create valid messages structure and senders

USAGE:
base and utils are automatically imported
import agrobolt.MODULE and use the required methods

IMPORTANT:
agrobolt.utils.allTheDevices is hardcoded and not up to date, use with caution



"""
import agrobolt.base as base
import agrobolt.utils as utils
#import agrobolt.decode as decode
#import agrobolt.web as web
#import agrobolt.parsing as parsing