.. Agrobolt documentation master file, created by
   sphinx-quickstart on Thu Sep 27 09:47:19 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Agrobolt's documentation
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



This is Agrobolt master package, contains python methods, classes and definitions useful across numerous implementations

If it must be used more than once or if it can be reused to another application it deserves to be here

* **base** : definitions, classes and Enums
* **utils** : static utilites, to be used and reused in other modules
* **decode** : data decoding and conversion, transform sensor raw measurements to processed and useful data
* **parsing** : data parsing from backup files, for backups or calc sheets
* **web** : data transmission, create valid messages structure and senders

Usage
===================
base and utils are automatically imported

import agrobolt.MODULE and use the required methods


base
===================
.. automodule:: agrobolt.base
   :members:
   :undoc-members:

utils
===================
.. warning::
   allTheDevices might not be up to date, check with the official SensorList document
.. automodule:: agrobolt.utils
   :members:

decode
===================
.. automodule:: agrobolt.decode
   :members:

web
===================
.. automodule:: agrobolt.web
   :members:
   

parsing
===================
.. automodule:: agrobolt.parsing
   :members: